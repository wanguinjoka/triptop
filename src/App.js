
import './App.css';
import Navbar from './components/Navbar';
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Search from './components/Search';
// import Tripgrid from './components/Tripgrid';
import Footer from './components/Footer';
import Home from './pages/Home';
import AddTrip from './pages/AddTrip';
import Trip from './pages/Trip';

function App() {
  return (
    <div className="App">
      <BrowserRouter> 
           <Navbar/>
           {/* <Search /> */}
           {/* <Home /> */}
           {/* <Tripgrid/> */}
           {/* <Footer/> */}
           
        <Switch>

        <Route exact path="/">
          <Home />
          {/* <Search /> */}
        </Route>
        <Route path="/add">
          <AddTrip />
        </Route>
        <Route  path="/search">
          <Search />
        </Route>
        <Route  path="/trip/:id">
          <Trip />
        </Route>
      </Switch>
      <Footer/>
      </BrowserRouter>
     
     
    </div>
  );
}

export default App;
