import  firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyBD8jDwAWxfhnyduBGJIn-wzcPL4461Cxs",
    authDomain: "triptop-66d00.firebaseapp.com",
    projectId: "triptop-66d00",
    storageBucket: "triptop-66d00.appspot.com",
    messagingSenderId: "631277730491",
    appId: "1:631277730491:web:00ebd11750f623d5f2079e"
  };

  //   init firebase
firebase.initializeApp(firebaseConfig)

// init services
const projectFirestore = firebase.firestore()

const storage = firebase.storage();

export{ projectFirestore , storage }