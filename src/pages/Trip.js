import React from 'react';
import { useEffect, useState} from 'react';
import { projectFirestore } from '../firebase/config';
import { useParams } from 'react-router-dom';



export default function Trip({trips}) {
    const [data, setData] = useState(null)
  const [isPending, setIsPending] = useState(false)
  const [error, setError] = useState(null)


  const { id } = useParams()

  useEffect(() => {
    setIsPending(true)
    const unsub = projectFirestore.collection('trips')
                     .doc(id)
                     .onSnapshot((doc) => {
                         if(doc.exists){
                             setIsPending(false)
                             setData({ id: doc.id, ...doc.data()})
                         }else{
                             setIsPending(false)
                             setError('Could not find trip')
                         }
                     })
  
    return () => {
      unsub()
    };
  }, [id]);

  
  return (
       <div>
           {error && <p>{error}</p>}
           {isPending && <p>Loading ....</p>}
           {data && (
               <div key={data.id}>
               < img src={data.url} alt='img'/>
               <span>{data.place}</span> 
               <p>{data.when}</p>
               <p>{data.description}</p>
               </div>
           )}

       </div>);
  }
