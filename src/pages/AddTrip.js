import React,{  useState}from 'react';
import { projectFirestore , storage} from '../firebase/config';
import imageicon from '../assests/image.svg';
import { useHistory } from 'react-router-dom';

// styles
import './AddTrip.css'


export default function AddTrip() {
    const [place, setPlace] = useState('')
    const [when, setWhen] = useState('')
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [image, setImage] = useState(null);
    const [url, setUrl] = useState('');
    const [ progress, setProgress] = useState(0);
    const history = useHistory()

    const handleUpload = () =>{
        const uploadImage = storage.ref(`images/${image.name}`)
                                .put(image);
                uploadImage.on(
                    "state_changed",
                    snapshot => {
                        const progress = Math.round(
                            (snapshot.bytesTransferred / snapshot.totalBytes)* 100
                         );
                         setProgress(progress);

                    },
                    error => {
                        console.log(error);
                    },
                    () => {
                        storage
                        .ref("images")
                        .child(image.name)
                        .getDownloadURL()
                        .then( url => {
                            setUrl(url)
                        })
                    }
                )

    };

    const handleSubmit = async (e) => {
        e.preventDefault()
        
        const doc = { place,
                      title,
                      when,
                      description,
                      url}
       try{
           await projectFirestore.collection('trips')
            .add(doc)
            history.push('/')
       }catch(err){
           console.log(err)
           alert(err)
       }
    }
    
  return (
  <div>
      <h3>Add new trip to the travel expo</h3>
      <label>
          <span>Upload image </span>
          <input 
                  type='file'
                  multiple accept='images/*'
                  onChange={(e) => setImage(e.target.files[0])}
                   />
                   <button onClick={handleUpload}>Upload Photo</button>
          </label>
          <div className="progress-bar" style={{ width: progress + '%'}}>
             <progress value={progress} max='100'/>
        </div>
        <img src={ url || imageicon } alt="upload" />

      <form onSubmit={handleSubmit}>
          <label>
              <span>Place</span>
              <input 
                  type='text'
                  onChange={ (e) => setPlace(e.target.value)}
                  value={place} />
          </label>
          <label>
              <span>Title</span>
              <input 
                  type='text'
                  onChange={ (e) => setTitle(e.target.value)}
                  value={title} />
          </label>
          
          <label for="start">Start date:
              <input 
                  type='date'
                  id="start"
                  name="trip-start"
                //   value="2022-02-02"
                  min="2022-02-02" max="2022-02-31"
                  onChange={ (e) => setWhen(e.target.value)}
                  value={when} />
          </label>
          <label>
              <span>Tell the story</span>
          <textarea 
                  type='text'
                  onChange={ (e) => setDescription(e.target.value)}
                  value={description} />
          </label>
          <button>Add</button>
      </form>

  </div>);
}
