import React from 'react';
import search from '../assests/search.svg';
import cancel from '../assests/cancel_black.svg';
import './Search.css'
import { useState } from 'react';
import { useHistory } from 'react-router-dom';

export default function Search() {
      const[term, setTerm] = useState('')
      const history = useHistory()

      const handleSubmit = (e) => {
            e.preventDefault()
    
            history.push(`/search?q=${term}`) 
              }
  return (
  <div>
        <form onSubmit={handleSubmit}>
              <div className='wrap'>
        <img class="search-icon" src={search} alt='search'/>
        <input className="search" 
              placeholder="Search" 
              type="text"
              id='search'
              onChange={ e => setTerm(e.target.value)}
              required/>
        {/* <img class="cancel" src={cancel} alt='cancel'/> */}
        </div>
        </form>
  </div>);
}
