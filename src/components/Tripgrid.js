import React from 'react';
import './Tripgrid.css'
import { projectFirestore } from '../firebase/config';
import trashcan from '../assests/deleteicon.svg';
import { Link } from 'react-router-dom'



export default function Tripgrid({trips}) {

    if(trips.length === 0){
        return <div className='error'>No trips to load.....</div>
    }
    const handleClick = (id) => {     
        projectFirestore.collection('trips')
        .doc(id)
        .delete()
    }

    
  return (
       <div className='trip-list'> 
           {trips.map( trip => (
             <div key={trip.id} className='card'>
                 < img src={trip.url} alt='img' className='card-image'/>
                 <div className='card-text'>
                 <Link className='title' to={`/trip/${trip.id}`}><h2>{trip.title}</h2></Link>
                 <div className='place1'>
                 <h3 className='place'>{trip.place}</h3>
                 <span><h3 className='date'>Date: {trip.when}</h3></span>
                 </div>
                 
                 <div> {trip.description.substring(0, 100)}...</div>

                 </div>
                 <div className='card-stats'>
          <Link className='more' to={`/trip/${trip.id}`}>Read More</Link>
          <img
              className='delete'
              src={trashcan} alt='delete-icon'
              onClick={() => handleClick(trip.id)} />
              </div>
          </div>
      ))}

       </div>);
}
