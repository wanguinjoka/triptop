import React from 'react';
import { Link } from 'react-router-dom'

// styles
import './Navbar.css'
import Search from './Search';

export default function Navbar() {
  return (
  <div className='navbar'>
      <nav>
      <Link to="/"  className='brand'><h2>TripTop</h2></Link>
      <Search className='search'/>
          <Link to="/add" className='addbutton'><h3>Add trip</h3></Link>
          <Link to="/about" className='about'><h3>About</h3></Link>
          <Link to="/contact" className='contact'><h3>Contact me</h3></Link>
      </nav>
  </div>);
}
